%%%-------------------------------------------------------------------
%%% @author vlomshakov
%%% @copyright (C) 2013, spbau
%%% @doc bully algorithm (see http://www.cs.iastate.edu/~cs554/NOTES/Ch6-Election.pdf)
%%%
%%% @end
%%% Created : 13. Dec 2013 12:22
%%%-------------------------------------------------------------------


-module(main).

%% API
-export([start/2, regularAction/2, coordinatorAction/2, electionAction/2]).

start(MyIdx, Nodes) ->
  { Address, Id } = lists:nth(MyIdx, Nodes),
  io:format("start node (~w, ~w) ~n", [Address, Id]),
  register(Address, spawn(main, electionAction, [MyIdx, Nodes])).

regularAction(MyIdx, Nodes) ->
  { SelfAddress, SelfId } = lists:nth(MyIdx, Nodes),
  io:format("start regular (~w, ~w) ~n", [SelfAddress, SelfId]),
  receive
    { _, _, coordinatorMsg } ->
      io:format("recv coordinatoMsg (~w, ~w) ~n", [SelfAddress, SelfId]),
      regularAction(MyIdx, Nodes);
    { From, _, electionMsg } ->
      io:format("recv electionMsg (~w, ~w) ~n", [SelfAddress, SelfId]),
      {From, From} ! {SelfAddress, SelfId, okMsg},
      electionAction(MyIdx, Nodes)
  after 5000 ->
    io:format("next iter regular (~w, ~w) ~n", [SelfAddress, SelfId]),
    electionAction(MyIdx, Nodes)
  end.

coordinatorAction(MyIdx, Nodes) ->
  { SelfAddress, SelfId } = lists:nth(MyIdx, Nodes),
  io:format("start coordinator (~w, ~w) ~n", [SelfAddress, SelfId]),
  [ {DestAddress, DestAddress} ! {SelfAddress, SelfId, coordinatorMsg} || {DestAddress, DestId} <- Nodes, DestId < SelfId ],
  receive
    { From, _, electionMsg } ->
      io:format("recv electionMsg (~w, ~w) ~n", [SelfAddress, SelfId]),
      {From, From} ! {SelfAddress, SelfId, okMsg},
      electionAction(MyIdx, Nodes);
    { _, _, coordinatorMsg } ->
      io:format("recv coordinatorMsg (~w, ~w) ~n", [SelfAddress, SelfId]),
      regularAction(MyIdx, Nodes)
  after 4000 ->
    io:format("next iter coordinator (~w, ~w) ~n", [SelfAddress, SelfId]),
    coordinatorAction(MyIdx, Nodes)
  end.

electionAction(MyIdx, Nodes) ->
  { SelfAddress, SelfId } = lists:nth(MyIdx, Nodes),
  io:format("start election (~w, ~w) ~n", [SelfAddress, SelfId]),
  [ {DestAddress, DestAddress} ! {SelfAddress, SelfId, electionMsg} || {DestAddress, DestId} <- Nodes, DestId > SelfId ],
  receive
    { _, _, okMsg } ->
      io:format("recv okMsg (~w, ~w) ~n", [SelfAddress, SelfId]),
      regularAction(MyIdx, Nodes)
  after 5000 ->
    io:format("new coordinator (~w, ~w) ~n", [SelfAddress, SelfId]),
    coordinatorAction(MyIdx, Nodes)
  end.