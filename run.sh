#!/usr/bin/env bash

N=$1 #count nodes


erlc main.erl

Node="[{'node0@$(hostname)',1}"

for i in $(seq 2 $N)
do
    Node=$Node", {'node$i@$(hostname)', $i}"
done

Node=$Node"]"
echo $Node
for i in $(seq 1 $N)
do
     nameNode="node$i"
     erl -noshell -sname $nameNode -eval "main:start($i, $Node)." &
     echo "run "$nameNode
done

wait
